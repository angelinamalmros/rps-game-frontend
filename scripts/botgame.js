let botScore = 0;
let playerScore = 0;
let moves = 0;
let movesLeft = document.getElementById("movesLeft");
let result = document.querySelector(".result");
// choicesObj är ett objekt med spelarens möjliga val som sen
// också sätter resultatet beroende på motspelarens/datorns val
let choicesObj = {
    'rock': {               // när spelaren väljer rock
        'rock': 'draw',     // om datorn sen väljer rock, blir resultatet draw
        'scissors': 'win',  // om datorn sen väljer scissors, blir det win för spelaren
        'paper': 'lose'     // om datorn sen väljer paper, blir det lose för spelaren
    },
    'scissors': {
        'rock': 'lose',
        'scissors': 'draw',
        'paper': 'win'
    },
    'paper': {
        'rock': 'win',
        'scissors': 'lose',
        'paper': 'draw'
    }
}

movesLeft.innerHTML = `MOVES LEFT: ${5}`;

function checker(input) {  // input => ikonen spelaren klickar på
    let choices = ["rock", "paper", "scissors"];
    let num = Math.floor(Math.random() * 3);
    let botChoice = choices[num];

    movesLeft.innerHTML = `MOVES LEFT: ${4 - moves}`;
    document.getElementById("botMove").innerHTML = `BOT PICKED <span> ${choices[num].toUpperCase()} </span>`;
    document.getElementById("playerMove").innerHTML = `YOU PICKED <span> ${input.toUpperCase()} </span>`;

    compareChoices([input], [botChoice]);
    updateScore();
    if (moves === 5) {
        gameOver();
    }
}

// denna metoden tar in choicesObj objektet och kollar vilken input spelaren gjort,
// och kollar mot valet datorn gjort, och utifrån datorns val är resultatet redan
// besämt i choicesObj objektet, och resultatet skrivs ut med hjälp av switchen
function compareChoices([input], [botChoice]) {
    switch (choicesObj[input][botChoice]) {
        case 'win':
            result.innerHTML = "YOU WIN!"
            playerScore++;
            moves++;
            break;
        case 'lose':
            result.innerHTML = "YOU LOSE!"
            botScore++;
            moves++;
            break;
        default:
            result.innerHTML = "DRAW!"
            moves++;
            break;
    }
}

function updateScore() {
    document.getElementById("botScore").innerHTML = botScore;
    document.getElementById("playerScore").innerHTML = playerScore;
}

function gameOver() {

    if (playerScore > botScore) {
        result.innerHTML = "GAME OVER. PLAYER WON!"
    } else if (botScore > playerScore) {
        result.innerHTML = "GAME OVER. BOT WON!"
    } else {
        result.innerHTML = "GAME OVER. IT'S A DRAW!"
    }

    disableIcons();
}

function disableIcons() {
    document.getElementById("rock").disabled = true;
    document.getElementById("paper").disabled = true;
    document.getElementById("scissors").disabled = true;
}