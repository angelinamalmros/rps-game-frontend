# rps-game-frontend

## Refletkion

### ARBETETS GÅNG
Rent praktiskt började arbetet med att jag bestämde mig för att göra projektet på egen hand och inte med gruppen jag tillhörde. Detta skulle visa sig vara på gott och ont. Det positiva med att vara själv var att jag kunde göra precis som jag ville och fick spelet att se ut som jag hade tänkt utan att behöva kompromissa. Men det var stundtals väldigt kämpigt när jag körde fast och inte direkt hade någon att bolla med. Såhär i efterhand skulle jag nog säga att det ÄR bättre att jobba i team än solo, åtminstone när det kommer till större projekt.

Efter att jag gjort en enkel designskiss i Figma började jag skapa mina HTML-sidor och satte ut alla element jag ville ha. Sedan kopplade jag på CSS och började laborera. När jag fått layout och design nästan färdigt satte jag igång med att skriva spelet mot datorn i JavaScript. Fick googla en del för inspiration och tips men lyckades tillslut skriva en kod som jag tyckte såg bra ut och faktiskt gjorde det jag ville. 

Slutligen var det dags att koppla på min egen backend och det var här de riktiga problemen började. Jag hade så otroligt svårt att 'wrap my head around' vad fetch innebar och hur det faktiskt fungerade. Det blev många timmar av total förvirring, frustration och hopplöshet (typ jag-kommer-aldrig-fixa-detta tankar) innan poletten äntligen föll ner och jag förstod vad som hände i mina fetch-anrop. Den svåraste delen var att få till 'player vs player' spelet för jag hade tänkt att det bara var att använda samma kod som i 'vs dator' spelet men det funkade ju inte alls, då spellogiken redan var skriven i min backend.

Det har varit extremt lärorikt och även roligt nu mot slutet när jag känner en stolthet över att jag lyckades få allting att funka. Vissa saker som jag ville ha med fick jag exkludera (registrera/logga in, lägga till så man kan spela flera rundor i player vs player) men tänker att jag ska försöka lägga till det senare som ytterligare övning, nu när jag föstår mig lite bättre på fetch. I mappen 'not using atm' ligger mina filer för registrerings- och inloggningsformulär samt separat CSS-fil för dessa.

### DESIGN
Enkel designskiss som jag skapade i Figma finns i img-mappen (prototyp.png)! Det var det absolut första jag gjorde i detta projeketet. Jag ville ha en enkel design med lite retro-pixel-känsla. Använde sidan https://colorhunt.co/ för att hitta en färgpalett som passade det jag hade i åtanke - en ljus bakgrund med tre primära färger som skulle symbolisera sten, sax och påse. Jag ritade ikonerna själv i programmet paint.net och använde färgerna jag valt ut för att färglägga dem. 

Under tiden som jag stylade i CSS lade jag till media-queries för mindre skärmar samt mobiler. Kollade av och uppdaterade dessa efter varje gång jag implementerat något nytt, till exempel en rubrik eller ikoner, så att det skulle se bra ut om/när skrämstorleken minskar. Insåg att inte allt behövde justeras i media-queries för om jag gjorde 'på rätt sätt' i min huvudsakliga CSS-styling så hängde det mesta med ändå.

Ville ha med någon typ av animation men med tanke på att min design är ganska simpel (utåt sett åtminstone) så valde jag att endast lägga till en liten animation som gör att texten blinkar när reslutatet av ett spel skrivs ut. 

### ANVÄNDARVÄNLIGHET
Saker jag tänkt på för att min webbapplikation ska vara användarvänlig:
- Skrivit HTML korrekt med rätt typ av element och namngivning
- Alternativa bildtexter
- Det är anpassat för även mindre skärmar/mobiler
- Bilderna/ikonerna är i bra storlek som inte påverkar sidan laddningshastighet
- Alla sidor ser i princip likadana ut och hänger ihop
- Det är lättläst med färger som är snälla för ögat
- Välorganiserat, luftigt och lättnavigerat
- Detta har jag inte aktivt implementerat vad jag vet, kanske finns förinställt i vissa webbläsare, men när jag provade går det att navigera mellan sidor och spela genom att endast använda tangentbordet (tab och enter)